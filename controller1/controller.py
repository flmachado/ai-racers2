from itertools import product
from typing import List, Tuple, Any
import numpy as np
from random import randint

primes = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101,
          103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193)


class State:
    def __init__(self, sensors: list):
        self.sensors = sensors
        #self.old = sensors
    def compute_features(self) -> Tuple:
        """
        This function should take the raw sensor information of the car (see below) and compute useful features for selecting an action
        The car has the following sensors:
        
        self.sensors contains (in order):
            0 track_distance_left: 1-100
            1 track_distance_center: 1-100
            2 track_distance_right: 1-100
            3 on_track: 0 if off track, 1 if on normal track, 2 if on ice
            4 checkpoint_distance: 0-???
            5 car_velocity: 10-200
            6 enemy_distance: -1 or 0-???
            7 enemy_position_angle: -180 to 180
            8 enemy_detected: 0 or 1
            9 checkpoint: 0 or 1
           10 incoming_track: 1 if normal track, 2 if ice track or 0 if car is off track
           11 bomb_distance = -1 or 0-???
           12 bomb_position_angle = -180 to 180
           13 bomb_detected = 0 or 1
          (see the specification file/manual for more details)
        :return: A Tuple containing the features you defined
        """
        s = self.sensors
        delta = s[0] - s[2]

        turn_incoming = s[5]/(s[1] * 2)  # bigger than 1 -> need break, lower than 1 -> go on
        
        car_velocity = s[5] * s[10]

        on_track = s[3]

        return [delta , turn_incoming, car_velocity, on_track]
        #raise NotImplementedError("This method must be implemented")

    def discretize_features(self, features: Tuple) -> Tuple:
        """
        This function should map the (possibly continuous) features (calculated by compute features) and discretize them.
        :param features 
        :return: A tuple containing the discretized features
        """
        delta = features[0]
        f = [0,0,0,0]
        if delta < -65:
            f[0] = 0
        elif delta > 65:
            f[0] = 1
        else:
            f[0] = 2
        
        turn_incoming = features [1]

        if turn_incoming > 1.5:
            f[1] = 0
        elif turn_incoming > 1:
            f[1] = 1
        elif turn_incoming > 0.5:
            f[1] = 2
        else:
            f[1] = 3

        car_velocity = features[2]

        if   car_velocity > 200:
            f[2] = 0
        elif car_velocity > 150:
            f[2] = 1
        elif car_velocity > 90:
            f[2] = 2
        elif car_velocity > 50:
            f[2] = 3
        else:
            f[2] = 4

        f[3] = features[3]

        return f
        #raise NotImplementedError("This method must be implemented")

    @staticmethod
    def discretization_levels() -> Tuple:
        """
        This function should return a vector specifying how many discretization levels to use for each state feature.
        :return: A tuple containing the discretization levels of each feature
        """
        return [3,4,5,3]
        #raise NotImplementedError("This method must be implemented")

    def get_current_state(self):
        """
        :return: computes the discretized features associated with this state object.
        """
        features = self.discretize_features(self.compute_features())
        return features

    @staticmethod
    def get_state_id(discretized_features: Tuple) -> int:
        """
        Handy function that calculates an unique integer identifier associated with the discretized state passed as parameter
        :param discretized_features
        :return: unique key
        """

        terms = [primes[i] ** discretized_features[i] for i in range(len(discretized_features))]

        s_id = 1
        for i in range(len(discretized_features)):
            s_id = s_id * terms[i]

        return s_id

    @staticmethod
    def get_number_of_states() -> int:
        """
        Handy function that computes the total number of possible states that exist in the system, according to the
        discretization levels specified by the user.
        :return: 
        """
        v = State.discretization_levels()
        num = 1

        for i in (v):
            num *= i

        return num

    @staticmethod
    def enumerate_all_possible_states() -> List:
        """
        Handy function that generates a list with all possible states of the system.
        :return: List with all possible states
        """

        levels = State.discretization_levels()

        levels_possibilities = [(j for j in range(i)) for i in levels]

        return [i for i in product(*levels_possibilities)]


class QTable:
    def __init__(self):
        """
        This class is used to create/load/store your Q-table. To store values we strongly recommend the use of a Python
        dictionary.
        """
        self.table = {}
        all_states = State.enumerate_all_possible_states()

        for i in all_states:
            self.table[State.get_state_id(i)] = [0,0,1,0,0]
        #raise NotImplementedError()

    def get_q_value(self, key: State, action: int) -> float:
        """
        Used to securely access the values within this q-table
        :param key: a State object 
        :param action: an action
        :return: The Q-value associated with the given state/action pair
        """
        currentState = key.get_current_state()
        stateId = key.get_state_id(currentState)
        
        return self.table[stateId][action -1]
        #raise NotImplementedError()

    def set_q_value(self, key: State, action: int, new_q_value: float) -> None:
        """
        Used to securely set the values within this q-table
        :param key: a State object 
        :param action: an action
        :param new_q_value: the new Q-value to associate with the specified state/action pair
        :return: 
        """
        currentState = key.get_current_state()
        stateId = key.get_state_id(currentState)

        self.table[stateId][action -1] = new_q_value
        #raise NotImplementedError()

    @staticmethod
    def load(path: str) -> "QTable":
        """
        This method should load a Q-table from the specified file and return a corresponding QTable object
        :param path: path to file
        :return: a QTable object
        """
        qTable = QTable()

        qTable.table = np.load(path).item()
        return qTable
        #raise NotImplementedError()

    def save(self, path: str, *args) -> None:
        """
        This method must save this QTable to disk in the file file specified by 'path'
        :param path: 
        :param args: Any optional args you may find relevant; beware that they are optional and the function must work
                     properly without them.
        """
        np.save(path, self.table) 
        #raise NotImplementedError()


class Controller:
    def __init__(self, q_table_path: str):
        if q_table_path is None:
            self.q_table = QTable()
        else:
            self.q_table = QTable.load(q_table_path)
        self.epsylon = 0.10 # % of times that the program chooses the non-optimum action
        self.alpha = 0.05   # % of the current state reward will affect the q_table value
        self.gama = 0.990    # % of the future actions will affect on the current q_table value
        

    def update_q(self, new_state: State, old_state: State, action: int, reward: float, end_of_race: bool) -> None:
        """
        This method is called by the learn() method in simulator.Simulation() to update your Q-table after each action is taken
        :param new_state: The state the car just entered
        :param old_state: The state the car just left
        :param action: the action the car performed to get to new_state
        :param reward: the reward the car received for getting to new_state  
        :param end_of_race: boolean indicating if a race timeout was reached
        """
        old_state_id = old_state.get_state_id(old_state.get_current_state())
        new_state_id = new_state.get_state_id(new_state.get_current_state())
        
        new_state_possibles_actions = self.q_table.table[new_state_id]
        
        old_value = self.q_table.table[old_state_id][action -1]
        #print((self.alpha*(reward + (self.gama*(max(new_state_possibles_actions))) ) ))
        new_value = ((1-self.alpha) * old_value) + (self.alpha*(reward + (self.gama*(max(new_state_possibles_actions))) ) ) #(self.gama*(max(new_state_possibles_actions)))
        
        self.q_table.set_q_value(old_state,action,new_value)
        #print(old_value," novo: ",new_value, " reward :",reward," state: ",old_state_id," ",self.q_table.table[old_state_id][action -1])
        #raise NotImplementedError("This method must be implemented")
    def compute_reward(self, new_state: State, old_state: State, action: int, n_steps: int, end_of_race: bool) -> float:
        """
        This method is called by the learn() method in simulator.Simulation() to calculate the reward to be given to the agent
        :param new_state: The state the car just entered
        :param old_state: The state the car just left
        :param action: the action the car performed to get in new_state
        :param n_steps: number of steps the car has taken so far in the current race
        :param end_of_race: boolean indicating if a race timeout was reached
        :return: The reward to be given to the agent
        """
        reward = 0
        f = new_state.get_current_state()
        of = old_state.get_current_state()
        s = new_state.sensors
        os = old_state.sensors

        if f[0] == 0 or f[0] == 1:
            reward += 1
        else:
            reward += 100
        
        if   f[1] == 0:
            reward += 1
        elif f[1] == 1:
            reward += 100
        elif f[1] == 2:
            reward += 20
        else: 
            reward += 1

        if   f[2] == 0:
            reward += 5
        elif f[2] == 1:
            reward += 50
        elif f[2] == 2:
            reward += 40
        elif f[2] == 3:
            reward += 20
        elif f[2] == 4:
            reward += 5

        if f[3] == 1 or f[3] == 2:
            reward += 50
        else:
            reward += -2000
        if os[4] - s[4] > 0 and (os[9] == 0 or s[9] == 0):
            reward += -1000
        else:
            reward += 50

        return reward




        #raise NotImplementedError("This method must be implemented")

    def take_action(self, new_state: State, episode_number: int) -> int:
        """
        Decides which action the car must execute based on its Q-Table and on its exploration policy
        :param new_state: The current state of the car 
        :param episode_number: current episode/race during the training period
        :return: The action the car chooses to execute
        """
        stateId = new_state.get_state_id(new_state.get_current_state())
        stateValues = self.q_table.table[stateId]
        
        wanna_Boltzmann = False
        if wanna_Boltzmann:
            t = 10
            sum_actions = 0
            vector = [0,0,0,0,0]
            j = 0
            for i in stateValues:
                sum_actions += pow(np.e,i/t)
            for i in stateValues:
                vector[j] = pow(np.e,i/t)/sum_actions
                j +=1
            
            return 1 + np.random.choice(5,1 ,vector)[0]





        if(randint(0,100)>95):
            return randint(1,4)
        else:
            return np.argmax(stateValues) +1
       # raise NotImplementedError("This method must be implemented")
    
